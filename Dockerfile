FROM ubuntu:14.04.4

RUN apt-get update \
	&& apt-get install -y --no-install-recommends unzip wget \
	&& rm -rf /var/lib/apt/lists/*
    